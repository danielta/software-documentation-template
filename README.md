
Software Documentation Template
================================
Software documentation is a crucial component of the software development process for 
stakeholders, users and developers. 
It is essential to to produce accurate and easily understandable documentation to
facilitate maintenance, reproducibility and standardisation. 

The Software Documentation Template (STD) is an evolving document capturing the 
the relevant software engineering best practices in design, develop and maintain software.

To maximise portability and simplify the editing process, the content of the documentation 
is organised in a textual files following the Markdown and/or RestructuredText 
syntax and graphs (e.g. UML, flowchart).

This template is an extension of the the ReadTheDocs (RTD) template and therefore
your can be hosted out-of-the-box on the [ReadTheDocs](https://readthedocs.org/) website.
This format is compliant with [Sphinx](sphinx-doc.org) that is a tool that allow to 
transform documentation in RTD to the format of interest to the user: HTML, PDF, ePub, Latex, etc.


In recent versions RTD has extensions for including various diagrams and graphs
described by simple domain specific languages: PlantUML, GraphViz and Marmeid.




[![Documentation Status](https://readthedocs.org/projects/software-documentation-template/badge/?version=latest)](http://software-documentation-template.readthedocs.io/en/latest/?badge=latest)
      
Installation 
-----------------------------

Our preferred way to work with STD and RTD is to create a Python virtual 
environment with the necessary dependencies:

~~~bash

    # Create a python virtual environment with Python e.g. version 3.9 (change accordingly)
    virtualenv-3.9 venv-py39
	
    #When virtualenv command will be deprecated, use:
    python3.9 -m venv venv-py39
	        
    #Load environment
    source venv-py39/bin/activate
	    
    #Install all dependencies
    pip install -r requirements.txt
	        
    #When you do not need to edit anymore the documents exit from the virtual environment
    deactivate

~~~


Fork this Software Documentation Template from [Software Documentation Template Github page](https://github.com/tartarini/software-documentation-template)

and then clone on your system. 






Write you documentation
-----------------------
A base structure of a documentation is in the ```doc```.
It is organised hierarchically as a tree of folders and text docuements.
 Sphinx initially supported only ReST textual documents and later added also Markdown 
 support. ReST and Markdown files can be added to the toctree directive and are rendered 
 correctly. Include directive can be used to inject text from one file to another one 
 but be mindful that Sphinx associate the text processor depending ont the container 
 file extension (i.e. do not inject markdown text into ReST).

The document files can be just in ResT format but we haven't found a reasonable support
 for editing them with a live preview as instead is for Markdown. On the other 
 hand Markdown doesn't support file inclusion that we fill being a substantial 
 limitation.
 
Therefore we use Markdown for all the documentation file except for the
files containing the table of contents or including other docs.


~~~bash

# Generate documentation webpages
make clean && make html

# Open in a browser the documentation (on Linux)
firefox  _build/html/index.html 

# Open in a browser the documentation (on OSX)
open _build/html/index.html

~~~


Generate your documentation in any of your preferred formats (html, latex, pdf, epub)

    cd docs
    
    # Generate Html documentation
    make html
    
    # Open documentation with a Web browser
    firefox docs/_build/html/index.html



A reference for the languages used to write the documentation are:
[ReST](https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html)

IDE Editors
-----------
* PyCharm
* Eclipse




 Pycharm is a multi-platform and open source editor with two plugins helping our
  editing: Markdown split editor with one section for writing code and one for live preview; 
  the GfmBrowser supports page reloading upon change so any time you recompile 
  the documentation you see the final ReadTheDocs pages.


 Eclipse has been identified as a multi-platform and open source editor with 
 support for plantUML. Plugin supporting Markdown and ReST editing with live 
 preview should be available.


    



Deploy on ReadTheDocs
----------------------

ReadTheDocs hosts for you version of your documentation that is rebuild 
automatically everytime you commit a change.
Go to https://readthedocs.org/ and setup your account.
If your documentation is not a public git repository, use the 
[ReadTheDocs manual import](https://readthedocs.org/dashboard/import/manual/?)


Contribute
----------
Contributions are welcome. Open an Issue in the git repository and/or a
git pull request.

Support and bugs
----------------

If you are having issues, please let open an issue on this documentation
 git repository or email contributors.

License
-------

The project is licensed under the BSD license.
