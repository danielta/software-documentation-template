.. Read the Docs Template documentation master file, created by
   sphinx-quickstart on Tue Aug 26 14:19:49 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation template for ReadTheDocs.org
==========================================


.. toctree::
   :caption: Project Introduction
   :name: mastertoc
   :maxdepth: 5
   

   readme
   Introduction/index
   Demo/index
   
.. toctree::
   :caption: Design Documents   
   :maxdepth: 5
 
   
   Design_documents/requirements
   Design_documents/architecture
   Design_documents/code_organisation
   Design_documents/uml_diagrams/uml_diagrams
   Design_documents/sustainability
   Design_documents/backup_policy
   
.. toctree::
   :caption: User Documentation   
   :maxdepth: 5
      
   Developer_manual/index
   User_manual/index
   Tutorial/index
   
   installation
   
.. toctree::
   :caption: Appendixes   
   :maxdepth: 5
   
   Appendices/index
   Appendices/Glossary
   Appendices/mermaid_diagrams
   
   Credits/credits   
   Credits/history 












Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

