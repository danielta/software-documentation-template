.. _Developer_manual:

Developer Manual
================

#. Developer Community and interaction channels (e.g. email, slack, mailing lists, etc)
#. Roles and permission of the developers and stakeholders
#. Code organisation
#. Development Workflow
#. Testing and validation workflow
#. Coding Standards
   * Syntax convention
   * e.g. Golden Rules and "Street Fighters" document
   * Test Driven Development
#. Continuous Integration and continuous deployment
#. Quality Assurance and Testing
   * `PEP8 Coding style checker <https://pypi.python.org/pypi/flake8>`_
#. Documentation
   * in code documentation and link to online doxygen
   * ReadTheCode or public documentation
#. Bug report
#. Licensing
#. Suggested Readings

