
# Architecture


Describe here the project architecture in all his components using where possible
UML diagrams (see appendices for examples to use PlantUML or Mermaid tools to 
embed diagrams in this documentation) 

* data models
* business logic
* Interface with users and other systems
* Libraries dependencies
* etc