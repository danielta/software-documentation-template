Requirements
==============

Here must be annotated the requirements for development of the project:
 * Contractual features to be delivered
 * Quality assurance measures
 * Environment requirements to allow the software to work
 * API and interactions with other software components
 * Compliancy with software standard
 * Deliverables and timeline (e.g. see Gantt charts in appendices)